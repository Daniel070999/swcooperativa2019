<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Cuenta;
use App\Transacciones;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ClienteController extends BaseController
{
    public function index(Request $request){
    	$clientes = Cliente::all();
    	return response()->json($clientes, 200);
    }
    public function getCliente(Request $request, $cedula){
    	if($request->isJson()){
	    	$cliente = Cliente::where('cedula',$cedula)->get();
	    	if(!$cliente->isEmpty()){
	    		$status = true;
	    		$info = "data is listed successfully";
	    	}else{
	    		$status = false;
	    		$info = "data is not listed successfully";
	    	}
	    	return ResponseBuilder::result($status, $info, $cliente, 200);
    	}else{
    		$status = false;
	    	$info = "unauhtorized";
    		return ResponseBuilder::result($status, $info);
    	}
    }
     public function createCliente(Request $request){
	     	$cliente = new Cliente();
	     	$cliente->cedula = $request->cedula;
	     	$cliente->nombres = $request->nombres;
	     	$cliente->apellidos = $request->apellidos;
	     	$cliente->genero = $request->genero;
	     	$cliente->estaCivil = $request->estaCivil;
	     	$cliente->fechaNacimiento = $request->fechaNacimiento;
	     	$cliente->correo = $request->correo;
	     	$cliente->telefono = $request->telefono;
	     	$cliente->celular = $request->celular;
	     	$cliente->direccion = $request->direccion;
	     	$cliente->save();
	     	$cuenta = new Cuenta();
			$num1 = (rand(1,999999999));
	     	$cuenta->numero = $num1;
	     	$cuenta->estado = $request->estado;
	     	$cuenta->fechaApertura = $request->fechaApertura;
	     	$cuenta->tipoCuenta = $request->tipoCuenta;
	     	$cuenta->saldo = $request->saldo;
	     	$cuenta->cliente_id = $cliente->cliente_id;
	     	$cuenta->save();
	}
	public function realizarTransaccion(Request $request){
		if($request->isJson()){
			$cuenta = Cuenta::where('numero',$request->numero)->first();
			$transaccion = new Transacciones();
			if ($cuenta != null){
				$transaccion->fecha = $request->fecha;
				$transaccion->tipo = $request->tipo;
				$valor = $request->valor;
				if($transaccion->tipo == 'deposito'){
					$cuenta->saldo = $cuenta->saldo + $valor;
					$cuenta->save();
				}else if($transaccion->tipo == 'retiro'){
					$cuenta->saldo = $cuenta->saldo - $valor;
					$cuenta->save();
				}
				$transaccion->valor = $valor;
				$transaccion->descripcion = $request->descripcion;
				$transaccion->responsable = $request->responsable;
				$transaccion->cuenta_id = $cuenta->cuenta_id;
				$transaccion->save();
				$status=true;
				$info="transaction is donde";
			
			}else{
				$status=false;
				$info="transaction is not donde";
			}
		return ResponseBuilder::result($status,$info,$transaccion);
		}
	}
	

	public function modificarCliente(Request $request){
			$cliente = Cliente::where('cedula',$request->cedula)->first();
	     	$cliente->nombres = $request->nombres;
	     	$cliente->apellidos = $request->apellidos;
	     	$cliente->genero = $request->genero;
	     	$cliente->estaCivil = $request->estaCivil;
	     	$cliente->fechaNacimiento = $request->fechaNacimiento;
	     	$cliente->correo = $request->correo;
	     	$cliente->telefono = $request->telefono;
	     	$cliente->celular = $request->celular;
	     	$cliente->direccion = $request->direccion;
	     	$cliente->save();
	}
	public function eliminarCliente($cedula){
	    	$cliente = Cliente::where($cedula)->firstOrFail();
    		$cliente->delete();
	}
    
}
