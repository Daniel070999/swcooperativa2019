<?php

namespace App\Http\Controllers;

use App\Http\Helper\ResponseBuilder;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;

use App\User;

class UserController extends BaseController{

    public function login(Request $request){
    	$username = $request->username;
    	$password = $request->password;

    	$user = User::where('username',$username)->first();
    	//error_log($this->django_password_veryfy($password, $user->password));
    	//error_log($user->password);
    	if(!empty($user)){
    		if($this->django_password_veryfy($password, $user->password)){
    			$status = true;
    			$info = "User is correct";
    		}else{
	    		$status = false;
   				$info = "User is incorrect";
    		}
    	}else{
   			$status = true;
			$info = "User is incorrect";
    	}
    	return ResponseBuilder::result($status, $info);
    }

    public function django_password_veryfy(string $password, string $djangoHash): bool{
    	$pieces = explode('$', $djangoHash);
    	if (count($pieces) !== 4){
    		throw new Exception("ilegal hash format");
    	}
    	list($header, $iter, $salt, $hash) = $pieces;

    	if (preg_match('#^pbkdf2_([a-z0-9A-Z]+)$#', $header, $m)){
    		$algo = $m[1];
    	}else{
    		throw new Exception(sprintf("bad header ($s)", $header));
    	}
    	if(!in_array($algo, hash_algos())){
    		throw new Exception(sprintf("ilegal hash algorithm (%s)", $algo));
    	}
    	$calc = hash_pbkdf2(
    		$algo, $password, $salt, (int) $iter, 32, true
    	);
    	return hash_equals($calc, base64_decode($hash));
    }

    public function logout(Request $request){

    }
}
